import csv
import os 
from collections import defaultdict
import sys
import operator

master_lookup = {} #a dictionary of sets
total = 0
def get_item_sets(min_sup,data,pattern):
    item_set = defaultdict(int)
    for row in data:
        for item in row: 
            item_set[item] += 1
    
    L1 = {}
    for key in item_set.keys():
        if float(item_set[key])/total >= min_sup:
            L1[key] = item_set[key]
    prevL = L1
    newL = L1

    k = 2
    answer = {}
    while(len(newL)>0):
        #answer.update(newL) This update is not required as we are doing this in line 48
        print "L"+str(k-1)+" rules "+str(len(newL))
        candidates = generate_candidates(newL,k,pattern)
        prevL = dict(newL)
        newL = {}
        for candidate in candidates:
            candidate_str = candidate.split('$') 
            enuf_sup = True  
            # support_rows = master_lookup[candidate_str[0]]
            # for i,item in enumerate(1,candidate_str[1:]):
            #     support_rows = support_rows & master_lookup[item]
            #     if float(len(support_rows))/total < min_sup :
            #         enuf_sup = False
            #         break
            support_rows = master_lookup[candidate_str[-1]]
            support_rows = support_rows & master_lookup[candidate_str[-2]]
            if(k>2):
                #If k > 3 join using $ . Else use last element as is
                if(k>3):
                    remaining_items_key = '$'.join(candidate_str[:-2])
                else:
                    remaining_items_key = candidate_str[-3]
                #print 'KEY - ' + remaining_items_key
                support_rows = support_rows & master_lookup[remaining_items_key]

            if float(len(support_rows))/total >= min_sup:
                newL[candidate] = len(support_rows)
                #print "candidate: "+ candidate
                master_lookup[candidate] = support_rows

        # f1 = open('output.txt','a+')
        # print >>f1, newL
        # f1.close()
        print "NewL"+str(k)+":" +str(len(newL))
            
        answer.update(newL)
        k+=1  

    return answer

def add_to_mastertable(item_sets_str):
    item_sets = item_sets_str.split('$')
    support_rows = master_lookup[item_sets[0]]
    for item in item_sets[1:]:
        support_rows = support_rows & master_lookup[item]
    master_lookup[item_sets_str] = support_rows
    return support_rows

def get_rules(min_conf,item_sets):
    rules = []
    items = item_sets.keys()
    for item in items:
        numerator = len(master_lookup[item])
        left_items = item.split('$')
        for i,left_item in enumerate(left_items):
            remaining_items_key = list(left_items[0:i])
            remaining_items_key.extend(left_items[i+1:])
            remaining_items_key = '$'.join(remaining_items_key)
            if master_lookup.has_key(remaining_items_key) :
                denominator = len(master_lookup[remaining_items_key])
            else :
                denominator = len(add_to_mastertable(remaining_items_key))
            if(float(numerator)/denominator >= min_conf):
                rule = {}
                rule["rule"] = [remaining_items_key,left_item]
                #rule[remaining_items_key] = left_item
                rule["confidence"] = int(float(numerator)/denominator*100)
                rule["support"] = int(float(numerator)/total*100)
                rules.append(rule)
    return rules
        
def load_data(inputpath):
    with open(inputpath, 'rb') as csvfile:
        inputreader = csv.reader(csvfile, delimiter=',', quotechar='|')
        data = []
        for row in inputreader:
            data.append(row)
    global total
    total = len(data)
    
    return data

def preprocess_data(data):
    global master_lookup
        
    for i in range(len(data)):
        for j in range(len(data[i])):
            if(master_lookup.has_key(data[i][j])):
                master_lookup[data[i][j]].add(i)
            else:
                master_lookup[data[i][j]] = set()
                master_lookup[data[i][j]].add(i)

def get_column_value(item):
    if("|" in item):
        return 1
    elif("DAY" in item):
        return 2
    elif("12:00-1:00AM" in item):
        return 3
    elif("1:00-2:00AM" in item):
        return 4
    elif("2:00-3:00AM" in item):
        return 5
    elif("3:00-4:00AM" in item):
        return 6
    elif("4:00-5:00AM" in item):
        return 7
    elif("5:00-6:00AM" in item):
        return 8
    elif("6:00-7:00AM" in item):
        return 9
    elif("7:00-8:00AM" in item):
        return 10
    elif("8:00-9:00AM" in item):
        return 11
    elif("9:00-10:00AM" in item):
        return 12
    elif("10:00-11:00AM" in item):
        return 13
    elif("11:00-12:00PM" in item):
        return 14
    elif("12:00-1:00PM" in item):
        return 15
    elif("1:00-2:00PM" in item):
        return 16
    elif("2:00-3:00PM" in item):
        return 17
    elif("3:00-4:00PM" in item):
        return 18
    elif("4:00-5:00PM" in item):
        return 19
    elif("5:00-6:00PM" in item):
        return 20
    elif("6:00-7:00PM" in item):
        return 21
    elif("7:00-8:00PM" in item):
        return 22
    elif("8:00-9:00PM" in item):
        return 23
    elif("9:00-10:00PM" in item):
        return 24
    elif("10:00-11:00PM" in item):
        return 25
    elif("11:00-12:00AM" in item):
        return 26

def numberOfCommonItems(item1,item2):
    if(len(item1) != len(item2)):
        print "Item lengths should be same"
        return
    else:
        #make set of the items excluding the last item
        set1 = set(item1[:-1])
        set2 = set(item2[:-1])
        return len(set1 & set2)

def generate_candidates(prevL,k,pattern):
    keys_list = prevL.keys()
    C_k = set()
    print "Generating candidates for L"+str(k)
    if(pattern == "weekday"):
        for i in xrange(len(keys_list)):
            i_has_weekday_item = False
            item_str_i = list(keys_list[i].split('$'))
            for element in item_str_i:
                if(get_column_value(element) == 2):
                    i_has_weekday_item = True
                    break
            for j in xrange(i+1,len(keys_list)):
                j_has_weekday_item = False
                item_str_j = list(keys_list[j].split('$'))     
                if(i_has_weekday_item == False):
                    for element in item_str_j:
                        if(get_column_value(element) == 1):
                            j_has_weekday_item = True
                            break
                if(i_has_weekday_item == True or j_has_weekday_item == True):
                    if (numberOfCommonItems(item_str_i,item_str_j) == k-2) and (item_str_i[k-2] != item_str_j[k-2]):
                        new_key = list(item_str_i[:-1]) #add all except the last item
                        #check the order of the last items
                        if(get_column_value(item_str_i[k-2]) < get_column_value(item_str_j[k-2])):
                            new_key.append(item_str_i[k-2])
                            new_key.append(item_str_j[k-2])
                        else:
                            new_key.append(item_str_j[k-2])
                            new_key.append(item_str_i[k-2])
                        #new_key = set(item_str_i).copy()
                        #new_key.update(set(item_str_j))
                        #new_key = sorted(new_key)
                        C_k.add('$'.join(new_key))

    else:
        for i in xrange(len(keys_list)):
            #item_str_i = set(keys_list[i].split('$'))
            item_str_i = list(keys_list[i].split('$')) 
            for j in xrange(i+1,len(keys_list)):
                #item_str_j = set(keys_list[j].split('$'))
                item_str_j = list(keys_list[j].split('$'))     
                #if k-2 <= 0 or (len(item_str_i & item_str_j)) == k-2:
                if (numberOfCommonItems(item_str_i,item_str_j) == k-2) and (item_str_i[k-2] != item_str_j[k-2]):
                    new_key = list(item_str_i[:-1]) #add all except the last item
                    #check the order of the last items
                    if(get_column_value(item_str_i[k-2]) < get_column_value(item_str_j[k-2])):
                        new_key.append(item_str_i[k-2])
                        new_key.append(item_str_j[k-2])
                    else:
                        new_key.append(item_str_j[k-2])
                        new_key.append(item_str_i[k-2])
                    #new_key = set(item_str_i).copy()
                    #new_key.update(set(item_str_j))
                    #new_key = sorted(new_key)
                    C_k.add('$'.join(new_key))
    print "Finished generating candidates for L"+str(k)
    print "Number of L"+ str(k) + " candidate " + str(len(C_k))
    return C_k

def apriori(data_filepath,min_sup,min_conf,pattern):
    dir_path = os.path.dirname(os.path.realpath(__file__)) 
    inputpath = dir_path + "/" +data_filepath
    data = load_data(inputpath)
   
    preprocess_data(data)
    item_sets = get_item_sets(min_sup,data,pattern)
    
    sorted_frequent_item_sets = []
    for item in item_sets:
        item_dict = {}
        item_dict['itemset'] = item.split("$")
        item_dict['support'] = int(float(len(master_lookup[item]))/total*100)
        sorted_frequent_item_sets.append(item_dict)


    sorted_frequent_item_sets = sorted(sorted_frequent_item_sets,key=operator.itemgetter('support'),reverse=True)
    
    print "Writing item sets into output.txt"
    with open("output.txt", "w") as outputfile:
        outputfile.write("==Frequent itemsets (min_sup="+str(min_sup*100)+"%)")
        outputfile.write("\n")
        outputfile.write("\n")
        for item in sorted_frequent_item_sets:
            outputfile.write(str(item['itemset'])+",\t"+str(item['support'])+"%")
            outputfile.write("\n")
            outputfile.write("\n")
    
    print "Generating Rules"

    rules = get_rules(min_conf,item_sets)
    conf_score = sorted(rules,key=operator.itemgetter('confidence'),reverse=True) 
    
    print "Writing rules into output.txt"
    with open("output.txt", "a") as outputfile:
        outputfile.write("==High-confidence association rules (min_conf="+str(min_conf*100)+"%)")
        outputfile.write("\n")
        outputfile.write("\n")
        for item in conf_score:
            outputfile.write(str(item['rule'][0].split('$'))+" => "+str(item['rule'][1]))
            outputfile.write(",\t(Conf: "+str(item['confidence'])+"%, Supp: "+str(item['support'])+"%)")
            outputfile.write("\n")
            outputfile.write("\n")
    return rules