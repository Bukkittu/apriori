GROUP NAME
==========
Project 3 Group 22

TEAM MEMBERS
=============
Avinash N Bukkittu ab4377
Bhavana R br2508

Detailed Description of the dataset
===================================
a) We used Traffic Volume Counts collected by Department of Transportation (DOT) for New York Metropolitan Transportation Council (NYMTC). The link is https://data.cityofnewyork.us/NYC-BigApps/Traffic-Volume-Counts-2011-2012-/wng2-85mv. The dataset is available in CSV format. In this dataset, we have traffic volume counts data for a few routes collected over a span of nearly 45 days on an hourly basis. First column is ID which serves as a primary for this dataset. Next column, GIS ID uniquely identifies a route. Each route belongs to a roadway as mentioned in the third column, Roadway Name. The next two columns, From and To are source and destination of the route respectively. The Direction column is used to indicate the direction of travel. For example, a route from UNION PLACE to VAN DUZER STREET has the direction NB. This implies the same route in reverse direction i.e from VAN DUZER STREET to UNION PLACE is SB. Thus, the (GIS ID + Direction) can be used to uniquely identify a route. The next column Date, mentions the date on which the traffic data was captured. The next 24 columns are 1-hour intervals indicating the traffic counts in that hour.

b) We processed the traffic count data in the following way
	- We used (GIS ID + Direction) to uniquely identify each row. Thus GIS_ID|DIRECTION is our primary key.
	- We converted each date to their corresponding day of the week. For example 9th January 2012 was converted to MONDAY. The idea behind this is that the traffic patterns are better observed over the days of the week rather than dates.
	- We stored the entire raw traffic volume counts in an array L in sorted order and plotted a histogram to observe the distribution of the vehicle counts. Let the size of the array be N
	- We divided this data into five zones. In the sorted array L, we checked the values at (N/5), (2N/5), (3N/5) and (4N/5). Each vehicle count c, was mapped with following rule
		-- if (v <= L[N/5]) then zone = VERY_LOW
		-- if (v <= L[2N/5] and v > L[N/5]) then zone = LOW
		-- if (v <= L[3N/5] and v > L[2N/5]) then zone = MEDIUM
		-- if (v <= L[4N/5] and v > L[3N/5]) then zone = HIGH
		-- if (v > L[4N/5]) then zone = VERY_HIGH
	With the given dataset size, dividing the data into five zones seems logical and also helps us differentiate patterns with VERY_LOW and VERY_HIGH values from LOW and HIGH values respectively.

	Using the above steps we generated INTEGRATED_DATASET.csv file.

c) Using the above dataset, we hope to find traffic patterns. Ideally, we hope that this dataset helps us find rush hours for different days of the week. For example, on Friday the rush hours are say 6:00pm-7:00PM or 8:00PM-9:00PM. This is the motivation behind choosing this dataset.

How to Run the Program?
=======================
Run the Apriori algorithm by issuing the following command from the terminal
	python Main.py INTEGRATED_DATASET.csv <min_support> <min_confidence>

Run a version of Apriori algorithm to find only the weekday patterns by issuing the following command from the terminal (The variation is described in the next section.)
	python Main.py INTEGRATED_DATASET.csv <min_support> <min_confidence> --pattern weekday

Internal Design of the Project
==============================
To quickly find if a candidate itemset is frequent we are using a map (named master_lookup) of key-value pairs. The key is the stringified version of the itemset and value is the list of row indexes in the csv that contain the itemset. For example if the itemset {I1,I2,I3} is in 5th and 6th rows then the corresponding map entry is the following
	KEY = "I1$I2$I3" (stringified the itemset using $ as a separator)
	VALUE = [5,6]
When we run the program, it first preprocesses the data to store all 1-itemset as a key in master_lookup along with the list of row indexes which contain these items as the corresponding values. This is done in the method - preprocess_data()
As mentioned in the reference paper, http://www.cs.columbia.edu/~gravano/Qual/Papers/agrawal94.pdf, the items in the itemsets are ordered by their column numbers.
The Apriori algorithm is implemented in three modules.
	- Candidate Generation - As is the standard procedure, Lk is used to generate (k+1)-itemset candidates. Every itemset in Lk is compared with every other itemset, if the first (k-1) items are common between two items p and q, then they are used to generate an itemset in C(k+1). The logic for candidate generation is found in the method - generate_candidates()
	- Candidate Filtering to generate frequent itemsets - This procedure gets the candidate items as the inputs. For each candidate itemset, we use the master_lookup map to determine if the candidate is frequent or not. If it is frequent then we add it to master_lookup so that we can use this data in subsequent iterations. This data is also used in the next module to calculate confidence. This logic is found in - get_item_sets()
	- Rule Generation -  Once we get all frequent itemsets, we call the rule generation module. This module takes as input the frequent itemsets. For each frequenct itemset, it iteratively check the confidence of the rule with one item on RHS i.e if the frequent itemset is {I1,I2,I3}, it iteratively checks for (I1,I2) => I3, (I1,I3) => I2 and (I2,I3) => I1. To calculate confidence, we retrieve the support value of the itemset from master_lookup. For each rule we divide this by the support value of the LHS retrieved from master_lookup. The rule that passes this confidence test, is stored in an array along with their support and confidence values. The list of such rules is returned by the module. This logic is found in - get_rules()

	Variation in the Apriori Algorithm
	==================================
	Since 24 columns of each row is a time-interval, most of the frequent itemsets will contain only time-intervals. This speaks to the fact that traffic is usually consistent over all weekdays and routes. If we want to find patterns with weekdays, say for example, how the traffic is on MONDAY then it is important that we find frequent items with exactly one item being a weekday. Then it becomes irrelevant to consider itemsets with only time-intervals. Thus, we have also implemented a version of Apriori algorithm where we consider only those itemsets with at least one item as a weekday. (L1 itemsets are an exception). Thus, we perform the candidate generation in the following way. L1, the set of frequent 1-itemset is formed as before. For generating any C(k+1) candidate we consider two k-itemsets p and q such that one of the itemset either p or q should contain a weekday as an item in the itemset. If any one of them contains, then we check if the first (k-1) are common, and if this test also passes then we use the itemsets p and q to generate (k+1)-itemset. The rest of the algorithm is implemenetd as the standard version. The logic of this variation is found in - generate_candidates()

Command Line Specification
==========================
python Main.py data/Traffic_Volume_Transformed.csv 0.07 0.8 --pattern weekday
	- This min_support value gives us rules where we can observe very high and very low traffic patterns with high confidence. The min_support which will give meaningful weekday results should be below 10% as at most 10% of the data contains a single weekday.