import csv
import os
import datetime

dir_path = os.path.dirname(os.path.realpath(__file__)) 
inputpath = dir_path+"/data/Traffic_Volume.csv"
outputpath = dir_path+"/data/Traffic_Volume_Transformed.csv"
headers = []
rows = []

def get_day_mapping(val):
    if(val == 0):
        return "MONDAY"
    elif(val == 1):
        return "TUESDAY"
    elif(val == 2):
        return "WEDNESDAY"
    elif(val == 3):
        return "THURSDAY"
    elif(val == 4):
        return "FRIDAY"
    elif(val == 5):
        return "SATURDAY"
    elif(val == 6):
        return "SUNDAY"

with open(inputpath, 'rb') as csvfile:
    inputreader = csv.reader(csvfile, delimiter=',')
    #for row in spamreader:
    headers = inputreader.next()
    print headers 
    for row in inputreader:
        for i in xrange(7,len(headers)):
            if(row[i] == ''):
                continue
            
            val = int(row[i])
            
            if(val <= 38):
                row[i] = "VERY_LOW"
            elif(val <= 116):
                row[i] = "LOW"
            elif(val <= 232):
                row[i] = "MED"
            elif(val <= 400):
                row[i] = "HIGH"
            else:
                row[i] = "VERY_HIGH"
        rows.append(row)

with open(outputpath, 'wb') as csvfile:
    outputwriter = csv.writer(csvfile, delimiter=',')
    for row in rows:
        newrow = []
        newrow.append(row[1]+"|"+row[5])
        newrow.append(row[6])
        day = get_day_mapping(datetime.datetime.strptime(row[6],'%m/%d/%y').date().weekday())
        newrow.append(day)
        for i in xrange(7,len(headers)):
           newrow = list(newrow)
           if(row[i]!='' and row[i]!='0'):
            headers[i] = headers[i].replace(' ','') #need to remove all whitespaces between headers
            newrow.append(headers[i]+"#"+row[i])
            
        outputwriter.writerow(newrow)

