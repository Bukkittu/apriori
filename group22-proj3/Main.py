import sys
import argparse
import Apriori

def cli():
    formatter = argparse.ArgumentDefaultsHelpFormatter
    parser = argparse.ArgumentParser(formatter_class=formatter)

    parser.add_argument(
        'data_filepath',
        type=str,
        help='Relative path to data file')
    
    parser.add_argument(
        'min_sup',
        type=float,
        help='Minimum support')

    parser.add_argument(
        'min_conf',
        type=float,
        help='Minimum confidence')

    parser.add_argument(
        '--pattern',
        type=str,
        default='all',
        help='Type of patterns')
    args = parser.parse_args()
    #print args.type
    return args.data_filepath,args.min_sup,args.min_conf,args.pattern

data_filepath,min_sup,min_conf,pattern = cli()

def main():
    if( min_sup < 0 or min_sup > 1 ):
        print "Invalid support value"
        sys.exit()
    if( min_sup < 0 or min_sup > 1 ):
        print "Invalid coverage value"
        sys.exit()

    rules = Apriori.apriori(data_filepath,min_sup,min_conf,pattern)    
    print 'Generated '+str(len(rules))+' rules. Check output.txt for rules'
    #print Apriori.apriori(data_filepath,min_sup,min_conf,pattern)
	

if __name__ == "__main__":main()